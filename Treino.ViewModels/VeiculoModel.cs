﻿namespace Treino.ViewModels
{
    public class VeiculoModel
    {
        public string name { get; set; }
        public string description { get; set; }
        public string vehicle_class { get; set; }
        public string length { get; set; }
        public string title { get; set; }
        //public PessoaEntity pilot { get; set; }
        //public virtual ICollection<FilmeEntity> films { get; set; }
        public string url { get; set; }
    }
}
